import tornado.web
import tornado.httpserver
import tornado.ioloop
import tornado.gen
from tornado.options import define, options

import qrcode

import logging
import base64
import StringIO

class QrHandler(tornado.web.RequestHandler):

    @tornado.web.asynchronous
    def get(self, content):
        try:
            content = base64.b64decode(content)
        except:
            raise tornado.web.HTTPError(400)

        img = qrcode.make(content, border=3)
        buf = StringIO.StringIO()
        img.save(buf, 'PNG')
        im = buf.getvalue()
        buf.close()

        self.set_header("Content-Type", "image/png")
        self.write(im)
        self.finish()

if __name__ == "__main__":
    define("port", help="run on the given port", type=int)
    options.parse_command_line()

    logging.basicConfig(level=logging.INFO,
    format='%(asctime)s    %(process)-6d    %(levelname)-6s    %(name)s    %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S')

    app_settings = dict(
    )

    app = tornado.web.Application([
        (r'^/qrcodes/(?P<content>[\w=]+).png$', QrHandler)
    ], **app_settings)

    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()
